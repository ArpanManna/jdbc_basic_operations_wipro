package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class Scenario_2 {

	
	static String getIncorrectAttempts(String userID)
	{
		int noOfAttempts=-1;
		String s="";
		try {
			Connection myConn=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_basic_db","root","welcome");
			Statement myStmt=myConn.createStatement();
			String sql="SELECT IncorrectAttempts FROM details WHERE UserId = ?";
			PreparedStatement prepStmt = myConn.prepareStatement(sql);
			prepStmt.setString(1, userID);
			ResultSet myRs=prepStmt.executeQuery();
			
			while(myRs.next()){
				noOfAttempts=myRs.getInt("IncorrectAttempts");
			}
			if(noOfAttempts==0)
				s="No Incorrect Attempt";
			else if(noOfAttempts==1)
				s="One time";
			else
				s="Incorrect Attempt Exceeded";
			
		}
		catch(Exception e){
			System.out.println("Connection could not be made");
		}
		return s;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String incorrectAttempt=getIncorrectAttempts("AB1001");
		System.out.println("Incorrect Attempt Of UserId: "+incorrectAttempt);
		
	}

}
