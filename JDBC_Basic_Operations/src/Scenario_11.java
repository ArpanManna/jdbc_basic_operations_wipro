package jdbc;
import java.sql.*;
import java.util.ArrayList;

class UserBE
{
	private String ID;
	private String password;
	private String name;
	private int incorrectAttempts;
	private int lockStatus;
	private String userType;
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIncorrectAttempts() {
		return incorrectAttempts;
	}
	public void setIncorrectAttempts(int incorrectAttempts) {
		this.incorrectAttempts = incorrectAttempts;
	}
	public int getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(int lockStatus) {
		this.lockStatus = lockStatus;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	
}

public class Scenario_11 {

	
	 static String[] getNames()
	 {
		 String[] arr=new String[100];
		 int i=0;
			try {
				Connection myConn=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_basic_db","root","welcome");
				Statement myStmt=myConn.createStatement();
				String sql ="SELECT Name FROM details";
				//PreparedStatement prepStmt = myConn.prepareStatement(sql);
				//prepStmt.setString(1,userType); 
				ResultSet myRs=myStmt.executeQuery(sql);
				while(myRs.next()) {
					
					arr[i++]=myRs.getString("Name");
					
					//array.add(myRs.getString("UserID"),myRs.getString("Password"),myRs.getString("Name"),myRs.getInt("IncorrectAttempts"),myRs.getInt("LockStatus"),myRs.getString("UserType"));
				}
			}	
			catch(Exception e){
				System.out.println("Connection could not be made");
			}
			return arr;
	 }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
		String[] array=new String[100];
		array=getNames();
		int i=0;
		for(i=0;i<array.length;i++)
			if(array[i]!=null)
			System.out.println(array[i]);
	}

}


