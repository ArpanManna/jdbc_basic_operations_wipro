package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

class User
{
	private String ID;
	private String password;
	private String name;
	private int incorrectAttempts;
	private int lockStatus;
	private String userType;
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIncorrectAttempts() {
		return incorrectAttempts;
	}
	public void setIncorrectAttempts(int incorrectAttempts) {
		this.incorrectAttempts = incorrectAttempts;
	}
	public int getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(int lockStatus) {
		this.lockStatus = lockStatus;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	
}


public class Scenario_10 {
	
	 static ArrayList<User> storeAllRecords()
	 {
		 ArrayList<User> array=new ArrayList<User>();
			try {
				Connection myConn=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_basic_db","root","welcome");
				Statement myStmt=myConn.createStatement();
				String sql ="SELECT * FROM details";
				//PreparedStatement prepStmt = myConn.prepareStatement(sql);
				//prepStmt.setString(1,userType); 
				ResultSet myRs=myStmt.executeQuery(sql);
				while(myRs.next()) {
					User u=new User();
					u.setID(myRs.getString("UserID"));
					u.setPassword(myRs.getString("Password"));
					u.setName(myRs.getString("Name"));
					u.setIncorrectAttempts(myRs.getInt("IncorrectAttempts"));
					u.setLockStatus(myRs.getInt("LockStatus"));
					u.setUserType(myRs.getString("UserType"));
					array.add(u);
					
					//array.add(myRs.getString("UserID"),myRs.getString("Password"),myRs.getString("Name"),myRs.getInt("IncorrectAttempts"),myRs.getInt("LockStatus"),myRs.getString("UserType"));
				}
			}	
			catch(Exception e){
				System.out.println("Connection could not be made");
			}
			return array;
	 }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<User> al=new ArrayList<User>();
		al=storeAllRecords();
		int i=0;
		for(i=0;i<al.size();i++)
		{
			System.out.println(al.get(i).getID()+" "+al.get(i).getPassword()+" "+al.get(i).getName()+" "+al.get(i).getIncorrectAttempts()+" "+al.get(i).getLockStatus()+" "+al.get(i).getUserType());
		}
	}

}
