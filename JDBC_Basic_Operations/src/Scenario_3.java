package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

public class Scenario_3 {
	
	
	static String changeUserType(String userID)
	{
		int count=0;String s="";
		try {
			Connection myConn=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_basic_db","root","welcome");
			Statement myStmt=myConn.createStatement();
			String sql="UPDATE details SET UserType = 'Admin' WHERE UserID = ?";
			PreparedStatement prepStmt = myConn.prepareStatement(sql);
			prepStmt.setString(1, userID);
			int myRs=prepStmt.executeUpdate();
			
			
			if(myRs>=1)
				s="Update Success";
			else
				s="Update Failed";
			
		}
		catch(Exception e){
			System.out.println("Connection could not be made");
		}
		return s;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String updateMessage=changeUserType("TA1002");
		System.out.println("Updation details: "+updateMessage);
	}

}
