package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Scenario_4 {
	
	
static int getLockStatus() {
		
		int noOfRows=-1;
		try {
			Connection myConn=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_basic_db","root","welcome");
			Statement myStmt=myConn.createStatement();
			String sql="SELECT * FROM details WHERE LockStatus = 0";
			ResultSet myRs=myStmt.executeQuery(sql);
			myRs.last();
			noOfRows=myRs.getRow();
			 myRs.beforeFirst();
		}
		catch(Exception e){
			System.out.println("Connection could not be made");
		}
		return noOfRows;
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int rows=getLockStatus();
		System.out.println("No of rows: "+rows);
	}

}
