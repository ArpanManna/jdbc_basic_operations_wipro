package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

public class Scenario_6 {
	
	static String changePassword(String password)
	 {
		 String s="";
			try {
				Connection myConn=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_basic_db","root","welcome");
				Statement myStmt=myConn.createStatement();
				String sql="UPDATE details SET password = ? WHERE UserType = 'Admin'";
				PreparedStatement prepStmt = myConn.prepareStatement(sql);
				prepStmt.setString(1, password);
				//prepStmt.setString(2, id);
				int myRs=prepStmt.executeUpdate();
				if(myRs>0)
					s="Changed";
				else
					s="0";
				
			}
			catch(Exception e){
				System.out.println("Connection could not be made");
			}
			return s;
	 }

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String passwordChangedStatus=changePassword("welcome");
		System.out.println("Updation details: "+passwordChangedStatus);
	}

}
