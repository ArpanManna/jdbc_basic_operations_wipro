package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

public class Scenario_5 {
	
	
	static String changeName(String id, String name)
	{
		String s="";
		try {
			Connection myConn=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_basic_db","root","welcome");
			Statement myStmt=myConn.createStatement();
			String sql="UPDATE details SET name = ? WHERE UserID = ?";
			PreparedStatement prepStmt = myConn.prepareStatement(sql);
			prepStmt.setString(1, name);
			prepStmt.setString(2, id);
			int myRs=prepStmt.executeUpdate();
			if(myRs>0)
				s="Success";
			else
				s="Failure";
			
		}
		catch(Exception e){
			System.out.println("Connection could not be made");
		}
		return s;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String updationStatus=changeName("TA1003", "Sourav");
		System.out.println("Updation details: "+updationStatus);
		
	}

}
