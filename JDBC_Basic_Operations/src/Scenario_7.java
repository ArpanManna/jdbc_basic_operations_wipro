package jdbc;
import java.sql.*;

class UserBean
{
	private String ID;
	private String password;
	private String name;
	private int incorrectAttempts;
	private int lockStatus;
	private String userType;
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIncorrectAttempts() {
		return incorrectAttempts;
	}
	public void setIncorrectAttempts(int incorrectAttempts) {
		this.incorrectAttempts = incorrectAttempts;
	}
	public int getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(int lockStatus) {
		this.lockStatus = lockStatus;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	
}


public class Scenario_7 {
	
	static String addUser(UserBean ub)
	{
		String s="";
		try {
			Connection myConn=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_basic_db","root","welcome");
			Statement myStmt=myConn.createStatement();
			String sql ="INSERT INTO details VALUES(?, ?, ?, ?, ?, ?)";
			PreparedStatement prepStmt = myConn.prepareStatement(sql);
			prepStmt.setString(1,ub.getID());
			prepStmt.setString(2,ub.getPassword());
			prepStmt.setString(3,ub.getName());
			prepStmt.setInt(4, ub.getIncorrectAttempts());
			prepStmt.setInt(5, ub.getLockStatus());
			prepStmt.setString(6,ub.getUserType());
			
			int myRs=prepStmt.executeUpdate();
			if(myRs>0)
				s="Success";
			else
				s="Fail";
			
		}
		catch(Exception e){
			System.out.println("Connection could not be made");
		}
		return s;
	}
 

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		UserBean ub=new UserBean();
		ub.setID("TA1004");
		ub.setPassword("welcome");
		ub.setName("debal");
		ub.setIncorrectAttempts(5);
		ub.setLockStatus(1);
		ub.setUserType("System engineer");
		
		String s=addUser(ub);
		System.out.println(s);

	}

}
