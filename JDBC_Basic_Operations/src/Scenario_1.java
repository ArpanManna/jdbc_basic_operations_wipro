package jdbc;
import java.sql.*;
public class Scenario_1 {

	
	static String getUserType(String id)
	{
		String s="";
		try {
			Connection myConn=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_basic_db","root","welcome");
			Statement myStmt=myConn.createStatement();
			String sql="SELECT UserType FROM details WHERE UserId = ?";
			PreparedStatement prepStmt = myConn.prepareStatement(sql);
			prepStmt.setString(1, id);
			ResultSet myRs=prepStmt.executeQuery();
			
			while(myRs.next()){
				s=myRs.getString("UserType");	
			}
		}
		catch(Exception e){
			System.out.println("Connection could not be made");
		}
		return s;
		
	}
	 
	
	public static void main(String[] args) {
		// Scenario_1
		String userType=getUserType("AB1001");
		System.out.print("USER type: "+userType);
		
	}

}
